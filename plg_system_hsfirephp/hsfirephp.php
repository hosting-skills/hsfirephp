<?php
/**
 * @package     HSFirePHP!
 * @author      Hosting Skills a.s.b.l. - http://www.hosting-skills.lu
 * @copyright   Copyright (C) 2014-2021 Hosting Skills a.s.b.l.. All rights reserved.
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * HSFirePHP! System Plugin
 */
class plgSystemHSFirePHP extends JPlugin {
    /**
     * Load the language file on instantiation. Note this is only available in Joomla 3.1 and higher.
     * If you want to support 3.0 series you must override the constructor
     *
     * @var    boolean
     * @since  3.1
     */
    protected $autoloadLanguage = true;

    /**
     * The validated parameters filled with default values if necessary.
     *
     * @var  array
     */
    protected $parameters = array();

    /**
     * Constructor.
     *
     * @param  object  $subject  The object to observe
     * @param  array   $config   An array that holds the plugin configuration
     * @since  1.0
     */
    public function __construct( &$subject, $config ) {
        parent::__construct( $subject, $config );

        $defaults = array();

        /* Activation */

        $defaults['sitesection'] = 3;
        $defaults['debugsystem'] = false;

        /* Authorization */

        $defaults['user'] = '';
        $defaults['groups'] = array();
        $defaults['accesslevels'] = array();

        /* Options */

        $defaults['maxobjectdepth'] = 5;
        $defaults['maxarraydepth'] = 5;
        $defaults['maxdepth'] = 10;
        $defaults['usenativejsonencode'] = true;
        $defaults['includelinenumbers'] = true;
        $defaults['linenumberoffset'] = 0;

        /* Handling */

        $defaults['errorhandling'] = true;
        $defaults['exceptionhandling'] = true;
        $defaults['assertionhandling'] = true;

        /* Demo */

        $defaults['demo'] = false;

        /* Parameters */

        $parameters['sitesection'] = $this->params->get('sitesection', $defaults['sitesection']);
        $parameters['debugsystem'] = $this->params->get('debugsystem', $defaults['debugsystem']);

        $parameters['user'] = $this->params->get('user', $defaults['user']);
        $parameters['groups'] = $this->params->get('groups', $defaults['groups']);
        $parameters['accesslevels'] = $this->params->get('accesslevels', $defaults['accesslevels']);

        $parameters['maxobjectdepth'] = $this->params->get('maxobjectdepth', $defaults['maxobjectdepth']);
        $parameters['maxarraydepth'] = $this->params->get('maxarraydepth', $defaults['maxarraydepth']);
        $parameters['maxdepth'] = $this->params->get('maxdepth', $defaults['maxdepth']);
        $parameters['usenativejsonencode'] = $this->params->get('usenativejsonencode', $defaults['usenativejsonencode']);
        $parameters['includelinenumbers'] = $this->params->get('includelinenumbers', $defaults['includelinenumbers']);
        $parameters['linenumberoffset'] = $this->params->get('linenumberoffset', $defaults['linenumberoffset']);

        $parameters['errorhandling'] = $this->params->get('errorhandling', $defaults['errorhandling']);
        $parameters['exceptionhandling'] = $this->params->get('exceptionhandling', $defaults['exceptionhandling']);
        $parameters['assertionhandling'] = $this->params->get('assertionhandling', $defaults['assertionhandling']);

        $parameters['demo'] = $this->params->get('demo', $defaults['demo']);

        $parameters = $this->validate($parameters, $defaults);

        $this->parameters = $parameters;
    }

    /**
     * Load FirePHP
     * Set Options
     * Register Handlers
     * Render HSFirePHP! Queue
     */
    function onAfterInitialise() {
        if ($this->canRun()) {
            require_once __DIR__ . '/hsfirephp.class.php';
        } else {
            include_once __DIR__ . '/hsfirephp.empty.php';
            return;
        }

        $firephp = FirePHP::getInstance(true);
        $firephp->setOptions(
            array(
                'maxDepth' => $this->parameters['maxobjectdepth'],
                'maxObjectDepth' => $this->parameters['maxarraydepth'],
                'maxArrayDepth' => $this->parameters['maxdepth'],
                'useNativeJsonEncode' => $this->parameters['usenativejsonencode'],
                'includeLineNumbers' => $this->parameters['includelinenumbers'],
                'lineNumberOffset' => $this->parameters['linenumberoffset']
            )
        );
        $firephp->setBasePath(realpath($_SERVER['DOCUMENT_ROOT']));
        // Disable HSFirePHP! for HSChromeLogger! Demo
        if (!(substr($_SERVER['SERVER_NAME'], -17) == 'hosting-skills.lu' && substr($_SERVER['REQUEST_URI'], 0, 20) == '/demo/hschromelogger')) {
            if ($this->parameters['errorhandling']) {
                $firephp->registerErrorHandler();
                set_error_handler('FB::errorHandler');
            }
            if ($this->parameters['exceptionhandling']) {
                $firephp->registerExceptionHandler();
                set_exception_handler('FB::exceptionHandler');
            }
            if ($this->parameters['assertionhandling']) {
                ini_set('assert.exception', 'Off');
                assert_options(ASSERT_WARNING, 0);
                $firephp->registerAssertionHandler();
                assert_options(ASSERT_CALLBACK, 'FB::assertionHandler');
            }
        }

        $session = JFactory::getApplication()->getSession();
        $queue   = $session->get('hsfirephp.queue', array());
        foreach ($queue as $args) {
            $fn = array_shift($args);
            if ('exception' == $fn) {
                $fn = 'fb';
                $args = array(new ErrorException($args[0], $args[1], $args[2], $args[3], $args[4]));
            }
            call_user_func_array(array($firephp, $fn), $args);
        }
    }

    /**
     * Empty HSFirePHP! Queue
     * Render Demo on Frontend if enabled.
     */
    function onAfterRender() {
        if (!$this->canRun() || !class_exists('FirePHP')) {
            return;
        }

	    $app = JFactory::getApplication();
        $app->getSession()->set('hsfirephp.queue', null);

        if (!$this->parameters['demo'] || !JFactory::getApplication()->isClient('site')) {
            return;
        }

        $firephp = FirePHP::getInstance(true);
	    $content = $app->getBody();
        preg_match_all('/HSFirePHP!::(LOG|ERROR|EXCEPTION|ASSERTION)/', $content, $matches, PREG_PATTERN_ORDER);
        if (in_array('LOG', $matches[1])) {
            $firephp->group('HSFirePHP!::LOG', array('Color' => '#0000FF'));
            $firephp->log('Plain Message');
            $firephp->info('Info Message');
            $firephp->warn('Warn Message');
            $firephp->error('Error Message');
            $firephp->log('Message','Optional Label');
            $firephp->groupEnd();
        }
        if (in_array('ERROR', $matches[1]) && $this->parameters['errorhandling']) {
            $firephp->group('HSFirePHP!::ERROR', array('Color' => '#0000FF'));
            // E_ERROR: /
            // E_WARNING:
            if (version_compare(phpversion(), '8.0.0.all', '<')) {
                trigger_error('', E_ERROR);
            } else {
                $E_WARNING++;
            }
            // E_PARSE: /
            // E_NOTICE:
            if (version_compare(phpversion(), '8.0.0.all', '<')) {
                $E_NOTICE++;
            } else {
                date_default_timezone_set('INVALID');
            }
            // E_CORE_ERROR: /
            // E_CORE_WARNING: /
            // E_COMPILE_ERROR: /
            // E_COMPILE_WARNING: /
            // E_USER_ERROR:
            trigger_error('E_USER_ERROR', E_USER_ERROR);
            // E_USER_WARNING:
            trigger_error('E_USER_WARNING', E_USER_WARNING);
            // E_USER_NOTICE:
            trigger_error('E_USER_NOTICE', E_USER_NOTICE);
            // E_STRICT: no longer exists in php 7.0 and newer
            if (version_compare(phpversion(), '7.0.0.all', '<')) {
                HSFirePHP::E_STRICT();
            }
            // E_RECOVERABLE_ERROR: no longer exists in php 7.4 and newer
            if (version_compare(phpversion(), '7.4.0.all', '<')) {
                (string) new HSChromeLogger();
            }
            // E_DEPRECATED:
            if (version_compare(phpversion(), '7.0.0.all', '<')) {
                split(':', '');
            } elseif (version_compare(phpversion(), '7.1.0.all', '<')) {
                HSFirePHP::E_STRICT();
            } elseif (version_compare(phpversion(), '8.0.0.all', '<')) {
                mb_ereg_replace('', '', '', 'e');
            } else {
                $array = [0, 1, 2];
                usort($array, function (int $a, int $b): bool {
                    return $a > $a;
                });
	        }
            // E_USER_DEPRECATED:
            trigger_error('E_USER_DEPRECATED', E_USER_DEPRECATED);
            $firephp->groupEnd();
        }
        if (in_array('ASSERTION', $matches[1]) && $this->parameters['assertionhandling']) {
            $firephp->group('HSFirePHP!::ASSERTION', array('Color' => '#0000FF'));
	        if (version_compare(phpversion(), '7.2.0.all', '<')) {
                assert('2 < 1');
	        } else {
		        assert(true == false);
	        }
            $firephp->groupEnd();
        }
        $app->getSession()->set('hsfirephp.queue', null);
        if (in_array('EXCEPTION', $matches[1]) && $this->parameters['exceptionhandling']) {
            if (4 != JVersion::MAJOR_VERSION) {
                $firephp->group('HSFirePHP!::EXCEPTION', array('Color' => '#0000FF'));
            }
            throw new Exception('Uncaught Exception');
        }
        $app->setBody(preg_replace('/HSFirePHP!&amp;#58;&amp;#58;(LOG|ERROR|EXCEPTION|ASSERTION)/', 'HSFirePHP!&#58;&#58;$1', $content));
    }

    /**
     * Validate all $parameters and use the value from $defaults if validation fails.
     *
     * @param   array  $parameters  Retrieved values for the parameters of the plugin
     * @param   array  $defaults    Default values for the parameters of the plugin
     *
     * @return  array  Validated values for the parameters of the plugin
     */
    protected function validate($parameters, $defaults) {
        foreach ($parameters as $key => $value) {
            unset($parameters[$key]);
            $key = strtolower($key);
            $value = is_string($value) ? trim($value) : $value;
            $pattern = '';
            switch ($key) {
                case 'debugsystem':
                case 'usenativejsonencode':
                case 'includelinenumbers':
                case 'errorhandling':
                case 'exceptionhandling':
                case 'assertionhandling':
                case 'demo':
                    switch($value) {
                        case '1':
                        case 'true':
                        case  true :
                            $parameters[$key] = true;
                            break;

                        case '0':
                        case 'false':
                        case  false :
                            $parameters[$key] = false;
                            break;

                        default:
                            $parameters[$key] = $defaults[$key];
                            break;
                    }
                    break;

                case 'sitesection':
                    preg_match('/^[123]$/', $value, $matches);
                    if ($matches[0] == $value) {
                        $parameters[$key] = $value;
                    } else {
                        $parameters[$key] = $defaults[$key];
                    }
                    $parameters[$key] = (int) $parameters[$key];
                    break;

                case 'maxdepth':
                    $pattern = '\d|2';

                case 'maxobjectdepth':
                case 'maxarraydepth':
                case 'linenumberoffset':
                    preg_match('/^1'.$pattern.'0|\d$/', $value, $matches);
                    if ($matches[0] == $value) {
                        $parameters[$key] = $value;
                    } else {
                        $parameters[$key] = $defaults[$key];
                    }
                    $parameters[$key] = (int) $parameters[$key];
                    break;

                default:
                    $parameters[$key] = $value;
                    break;
            }
        }

        return $parameters;
    }

    /**
     * Are the requirements fulfilled to run HSFirePHP!?
     *
     * @return  boolean  True if HSFirePHP! can run.
     */
    protected function canRun() {
        $app = JFactory::getApplication();
        if ($app->isClient('administrator') && 1 == $this->parameters['sitesection']) {
            return false;
        }
        if ($app->isClient('site')  && 2 == $this->parameters['sitesection']) {
            return false;
        }
        if ($this->parameters['debugsystem'] && !JDEBUG) {
            return false;
        }

        if (('' != $this->parameters['user']             && '0' != $this->parameters['user'])             ||
            (is_array($this->parameters['groups'])       && count($this->parameters['groups'])       > 0) ||
            (is_array($this->parameters['accesslevels']) && count($this->parameters['accesslevels']) > 0)
           ) {
            $user = JFactory::getUser();
            $authorization = false;
            if ('' != $this->parameters['user'] && '0' != $this->parameters['user'] && $this->parameters['user'] == $user->id) {
                $authorization = true;
            }
            if (is_array($this->parameters['groups']) && count($this->parameters['groups']) > 0) {
                $intersection = array_intersect($user->getAuthorisedGroups(), $this->parameters['groups']);
                if (count($intersection) > 0) {
                    $authorization = true;
                }
            }
            if (is_array($this->parameters['accesslevels']) && count($this->parameters['accesslevels']) > 0) {
                $intersection = array_intersect($user->getAuthorisedViewLevels(), $this->parameters['accesslevels']);
                if (count($intersection) > 0) {
                    $authorization = true;
                }
            }
            if (!$authorization) {
                return false;
            }
        }

        return true;
    }
}

/**
 * Class used to generate an E_STRICT or an E_RECOVERABLE_ERROR error.
 */
class HSFirePHP {
    public function E_STRICT() {
    }
}