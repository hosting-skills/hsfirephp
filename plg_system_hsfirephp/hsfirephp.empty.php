<?php
/**
 * @package     HSFirePHP!
 * @author      Hosting Skills a.s.b.l. - http://www.hosting-skills.lu
 * @copyright   Copyright (C) 2014-2021 Hosting Skills a.s.b.l.. All rights reserved.
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Sends the given data to the FirePHP Firefox Extension.
 * The data can be displayed in the Firefox Web Console.
 *
 * @param   mixed    $object
 *
 * @return  boolean  TRUE
 *
 * @throws  Exception
 */
function fb() {
    return true;
}

class FB {
    /**
     * Enable and disable logging to Firefox Web Console
     *
     * @param   boolean  $enabled  TRUE to enable, FALSE to disable
     *
     * @return  void
     */
    public static function setEnabled($enabled) {
    }

    /**
     * Check if logging is enabled
     *
     * @return  boolean  TRUE if enabled
     */
    public static function getEnabled() {
        return false;
    }

    /**
     * Specify a filter to be used when encoding an object
     *
     * Filters are used to exclude object members.
     *
     * @param   string  $class   The class name of the object
     * @param   array   $filter  An array or members to exclude
     *
     * @return  void
     */
    public static function setObjectFilter($class, $filter) {
    }

    /**
     * Set some options for the library
     *
     * @param   array  $options  The options to be set
     *
     * @return  void
     */
    public static function setOptions($options) {
    }

    /**
     * Get options for the library
     *
     * @return  array  The options
     */
    public static function getOptions() {
        return array();
    }

    /**
     * Log object to Firefox Web Console
     *
     * @param   mixed    $object
     *
     * @return  boolean  TRUE
     *
     * @throws  Exception
     */
    public static function send() {
        return true;
    }

    /**
     * Start a group for following messages
     *
     * Options:
     *   Collapsed: [true|false]
     *   Color:     [#RRGGBB|ColorName]
     *
     * @param   string   $name
     * @param   array    $options  optional  Instructions on how to log the group
     *
     * @return  boolean  TRUE
     */
    public static function group($name, $options = null) {
        return true;
    }

    /**
     * Ends a group you have started before
     *
     * @return  boolean  TRUE
     *
     * @throws  Exception
     */
    public static function groupEnd() {
        return true;
    }

    /**
     * Log object with label to Firefox Web Console
     *
     * @param   mixed    $object
     * @param   string   $label
     *
     * @return  boolean  TRUE
     *
     * @throws  Exception
     */
    public static function log($object, $label = null) {
        return true;
    }

    /**
     * Log object with label to Firefox Web Console
     *
     * @param   mixed    $object
     * @param   string   $label
     *
     * @return  boolean  TRUE
     *
     * @throws  Exception
     */
    public static function info($object, $label = null) {
        return true;
    }

    /**
     * Log object with label to Firefox Web Console
     *
     * @param   mixed    $object
     * @param   string   $label
     *
     * @return  boolean  TRUE
     *
     * @throws  Exception
     */
    public static function warn($object, $label = null) {
        return true;
    }

    /**
     * Log object with label to Firefox Web Console
     *
     * @param   mixed    $object
     * @param   string   $label
     *
     * @return  boolean  TRUE
     *
     * @throws  Exception
     */
    public static function error($object, $label = null) {
        return true;
    }

    /**
     * Dumps key and variable to Firefox Web Console
     *
     * @param   string  $key
     * @param   mixed   $variable
     *
     * @return  boolean  TRUE
     *
     * @throws  Exception
     */
    public static function dump($key, $variable) {
        return true;
    }

    /**
     * Log a trace in the Firefox Web Console
     *
     * @param   string   $label
     *
     * @return  boolean  TRUE
     *
     * @throws  Exception
     */
    public static function trace($label) {
        return true;
    }

    /**
     * Log a table in the Firefox Web Console
     *
     * @param    string  $label
     * @param    string  $table
     *
     * @return  boolean  TRUE
     *
     * @throws  Exception
     */
    public static function table($label, $table) {
        return true;
    }

    /**
     * Queue using JSession
     *
     * @param   mixed  $object
     *
     * @return  void
     */
    private static function queue() {
    }

    /**
     * FirePHP's error handler
     *
     * Throws exception for each php error that will occur.
     *
     * @param  int     $errno
     * @param  string  $errstr
     * @param  string  $errfile
     * @param  int     $errline
     */
    public static function errorHandler($errno, $errstr, $errfile, $errline) {
    }

	/**
	 * FirePHP's exception handler
	 *
	 * Logs all exceptions to your Firefox Web Console and then stops the script.
	 *
	 * @param  Exception  $exception
	 *
	 * @throws  Exception
	 */
	public static function exceptionHandler($exception) {
	}

    /**
     * FirePHP's assertion handler
     *
     * Logs all assertions to your Firefox Web Console and then stops the script.
     *
     * @param  string  $file  File source of assertion
     * @param  int     $line  Line source of assertion
     * @param  mixed   $code  Assertion code
     * @param  string  $desc  Assertion description
     */
    public static function assertionHandler($file, $line, $code, $desc = null) {
    }
}