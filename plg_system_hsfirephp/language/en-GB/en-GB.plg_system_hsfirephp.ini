;/**
; * @package    HSFirePHP!
; * @author     Hosting Skills a.s.b.l. - http://www.hosting-skills.lu
; * @copyright  Copyright (C) 2014-2021 Hosting Skills a.s.b.l.. All rights reserved.
; * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
; */

PLG_SYSTEM_HSFIREPHP_ACTIVATION="<strong>Activation</strong>"
PLG_SYSTEM_HSFIREPHP_SITESECTION="Site Section"
PLG_SYSTEM_HSFIREPHP_SITESECTION_DESCRIPTION="In which sections of your site do you want to enable <em>HSFirePHP!</em>?"
PLG_SYSTEM_HSFIREPHP_SITESECTION_SITE="Site (front-end)"
PLG_SYSTEM_HSFIREPHP_SITESECTION_ADMINISTRATOR="Administrator (back-end)"
PLG_SYSTEM_HSFIREPHP_SITESECTION_BOTH="Both"
PLG_SYSTEM_HSFIREPHP_DEBUGSYSTEM="Depend on Debug System?"
PLG_SYSTEM_HSFIREPHP_DEBUGSYSTEM_DESCRIPTION="Enable only if the standard <em>Joomla!</em> Debug System is enabled?"

PLG_SYSTEM_HSFIREPHP_AUTHORIZATION="<strong>Authorization</strong>"
PLG_SYSTEM_HSFIREPHP_USER="User"
PLG_SYSTEM_HSFIREPHP_USER_DESCRIPTION="Enable <em>HSFirePHP!</em> for the selected user."
PLG_SYSTEM_HSFIREPHP_GROUPS="Groups"
PLG_SYSTEM_HSFIREPHP_GROUPS_DESCRIPTION="Enable <em>HSFirePHP!</em> for the selected groups."
PLG_SYSTEM_HSFIREPHP_ACCESSLEVELS="Access Levels"
PLG_SYSTEM_HSFIREPHP_ACCESSLEVELS_DESCRIPTION="Enable <em>HSFirePHP!</em> for the selected access levels."

PLG_SYSTEM_HSFIREPHP_OPTIONS="<strong>Options</strong>"
PLG_SYSTEM_HSFIREPHP_MAXOBJECTDEPTH="Maximum Object Depth"
PLG_SYSTEM_HSFIREPHP_MAXOBJECTDEPTH_DESCRIPTION="Maximum depth to traverse objects."
PLG_SYSTEM_HSFIREPHP_MAXARRAYDEPTH="Maximum Array Depth"
PLG_SYSTEM_HSFIREPHP_MAXARRAYDEPTH_DESCRIPTION="Maximum depth to traverse arrays."
PLG_SYSTEM_HSFIREPHP_MAXDEPTH="Maximum Depth"
PLG_SYSTEM_HSFIREPHP_MAXDEPTH_DESCRIPTION="Maximum depth to traverse mixed arrays/objects."
PLG_SYSTEM_HSFIREPHP_USENATIVEJSONENCODE="Use native JSON encoder"
PLG_SYSTEM_HSFIREPHP_USENATIVEJSONENCODE_DESCRIPTION="Select <em>No</em> to use JSON encoder included with <em>FirePHPCore</em> instead of <code>json_encode()</code>."
PLG_SYSTEM_HSFIREPHP_INCLUDELINENUMBERS="Include line numbers"
PLG_SYSTEM_HSFIREPHP_INCLUDELINENUMBERS_DESCRIPTION="Include <em>File</em> and <em>Line</em> information in message."
PLG_SYSTEM_HSFIREPHP_LINENUMBEROFFSET="Maximum Object Depth"
PLG_SYSTEM_HSFIREPHP_LINENUMBEROFFSET_DESCRIPTION="Maximum depth to traverse objects."

PLG_SYSTEM_HSFIREPHP_HANDLING="<strong>Error, Exception & Assertion Handling</strong>"
PLG_SYSTEM_HSFIREPHP_ERRORHANDLING="Register <em>HSFirePHP!</em> as error handler"
PLG_SYSTEM_HSFIREPHP_ERRORHANDLING_DESCRIPTION="Register <em>HSFirePHP!</em> as your error handler for <code>E_WARNING</code>, <code>E_NOTICE</code>, <code>E_USER_ERROR</code>, <code>E_USER_WARNING</code>, <code>E_USER_NOTICE</code>, <code>E_STRICT</code>, <code>E_RECOVERABLE_ERROR</code>, <code>E_DEPRECATED</code> and <code>E_USER_DEPRECATED</code> errors."
PLG_SYSTEM_HSFIREPHP_EXCEPTIONHANDLING="Register <em>HSFirePHP!</em> as exception handler"
PLG_SYSTEM_HSFIREPHP_EXCEPTIONHANDLING_DESCRIPTION="Register <em>HSFirePHP!</em> as your exception handler."
PLG_SYSTEM_HSFIREPHP_ASSERTIONHANDLING="Register <em>HSFirePHP!</em> driver as assert callback"
PLG_SYSTEM_HSFIREPHP_ASSERTIONHANDLING_DESCRIPTION="Register <em>HSFirePHP!</em> driver as your assert callback."

PLG_SYSTEM_HSFIREPHP_DEMO_FIELDSET="Demo"
PLG_SYSTEM_HSFIREPHP_DEMO_EXPLANATION="When the demo mode is enabled, the following tags will log to your <em>Firefox</em> or <em>Chrome</em> Web Console:<ul><li><strong>HSFirePHP!&#58;&#58;LOG</strong><br />You will see 5 messages (log, info, warn, error, log with optional label).</li><li><strong>HSFirePHP!&#58;&#58;ERROR</strong><br />You will see up to 9 errors (<code>E_WARNING</code>, <code>E_NOTICE</code>, <code>E_USER_ERROR</code>, <code>E_USER_WARNING</code>, <code>E_USER_NOTICE</code>, <code>E_STRICT</code>, <code>E_RECOVERABLE_ERROR</code>, <code>E_DEPRECATED</code>, <code>E_USER_DEPRECATED</code>).</li><li><strong>HSFirePHP!&#58;&#58;EXCEPTION</strong><br />You will see 1 uncaught exception. Execution will stop after the exception handler is called.<br /><span style='color: red'><strong>Warning:</strong> In <em>Joomla!</em> 3.x you will no longer be able to render the page this tag is used in.</span></li><li><strong>HSFirePHP!&#58;&#58;ASSERTION</strong><br>You will see 1 assertion (<code>2 < 1</code> or <code>true == false</code>) with result <code>FALSE</code>.</li></ul>"
PLG_SYSTEM_HSFIREPHP_DEMO="Enable Demo Mode?"
PLG_SYSTEM_HSFIREPHP_DEMO_DESCRIPTION="Enable logging to your <em>Firefox</em> or <em>Chrome</em> Web Console using special Demo Tags."